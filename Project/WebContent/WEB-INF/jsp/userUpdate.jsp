<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/userUpdate.css" type="text/css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>ユーザー更新画面</title>
</head>
<body>
	<header>
		<div class="header-name">
			<!--あとで利用者の名前を出力するように変更する-->
			ユーザー名 さん <a class="logout" href="LogoutServlet">ログアウト </a>
		</div>

	</header>
	<main>
	<h1 class="userUpdate-title">ユーザー情報更新</h1>
	<form action="UserUpdateServlet" method="post">
		<div class="userUpBox">
			<div class="userUpdate">
				<!-- TODO テキストボックスに保存されている内容を表示 -->
				<p>ログインID
				<div class="box">${loginId}</div>
				</p>
				<p>
					パスワード<input class="box" type="text" name="passwordA">
				</p>
				<p>
					パスワード(確認)<input class="boxA" type="text" name="passwordB">
				</p>
				<p>
					ユーザー名<input class="box" type="text" name="userName"
						value="${name }">
				</p>
				<p>
					生年月日<input class="box" type="date" name="birthDate"
						value="${birthDate }">
				</p>
				<input type="hidden" name="id" value="${id }"> <input
					type="submit" value="更新">
			</div>
		</div>


	</form>


	</main>


	<footer>
		<a class="return" href="UserListServlet"> 戻る </a>

	</footer>





</body>
</html>