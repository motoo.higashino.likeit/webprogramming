<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧画面</title>
<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
<!-- Jqeryの読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
<!-- BootstrapのJS読み込み -->
	<script src="js/bootstrap.min.js">
</script>
<!-- レイアウトカスタマイズ用個別CSS -->
<title>ユーザー一覧</title>
<link rel="stylesheet" href="css/userList.css" type="text/css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">


</head>
<body>
	<header>
		<div class="header-name">
			<!--あとで利用者の名前を出力するように変更する-->
			${userInfo.name}さん
			 <a class="logout" href="LogoutServlet">ログアウト
			</a>
		</div>

	</header>
	<main>
	<h1 class="mainTitle">ユーザー一覧</h1>
	<h4>${errMsg }</h4>
	<a class="newData" href="UserRegisterServlet"> 新規登録 </a>

	<form class="dataSearch" action="UserListServlet" method="post">
		<p>
			ログインID<input class="textbox" type="text" name="loginId"
				style="width: 357px;">
		</p>
		<p>
			ユーザー名<input class="textbox" type="text" name="userName"
				style="width: 357px;">
		</p>
		<p>
			生年月日<input type="date" name="dateA">～<input type="date"
				name="dateB">
		</p>
		<input type="submit" value="検索">

	</form>

	<!--ここにデータベースのテーブルを挿入する-->
	<table class="data-table">

		<tr>
			<th>ログインID</th>
			<th>ユーザー名</th>
			<th>生年月日</th>
			<th></th>
		</tr>
		<c:forEach var="User" items="${userList }">
			<tr>
				<td>${User.loginId }</td>
				<td>${User.name }</td>
				<td>${User.birthDate }</td>
				<td><a class="det" href="UserDetailSevlet?id=${User.id }">詳細</a>
					<c:if test="${userInfo.id==1||userInfo.id==User.id}">
						<a class="up" href="UserUpdateServlet?id=${User.id }">更新</a>
					</c:if>
					<c:if test="${userInfo.id==1 }">
						<a class="del" href="UserDeleteServlet?id=${User.id }">削除</a>
					</c:if></td>
			</tr>
		</c:forEach>
	</table>

	</main>
	<footer> </footer>


</body>
</html>