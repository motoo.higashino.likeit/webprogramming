<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/userDelete.css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>Insert title here</title>
</head>
<body>
	<header>
		<div class="header-name">
			<!--あとで利用者の名前を出力するように変更する-->
			${User.name } さん <a class="logout" href="LogoutServlet"> ログアウト </a>
		</div>

	</header>
	<main>
	<div class="delete">
		<h1>ユーザー削除確認</h1>
		<P>ログインID&emsp;${user.loginId }</P>
		<P>を本当に削除してよろしいでしょうか。</P>


		<form action="UserListServlet" method="get">
			<input type="submit" value="キャンセル">
		</form>
		<form action="UserDeleteServlet" method="post">
			<input type="hidden" name="id" value="${id }"> <input
				type="submit" value="OK">
		</form>
	</div>
	</main>


</body>
</html>