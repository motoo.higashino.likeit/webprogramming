<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/userRegister.css" type="text/css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>ユーザー登録画面</title>
</head>
<body>
	<header>
		<div class="header-name">
			<!--あとで利用者の名前を出力するように変更する-->
			${User.name } さん <a class="logout" href="LogoutServlet"> ログアウト </a>
		</div>

	</header>
	<main>
	<h1 class="register-title">ユーザー新規登録</h1>
	<div>${errMsg}</div>

	<form action="UserRegisterServlet" method="post">
		<div class="regibox">
			<div class="register">
				<p>
					ログインID<input class="box" type="text" name="loginId">
				</p>
				<p>
					パスワード<input class="box" type="text" name="passwordA">
				</p>
				<p>
					パスワード(確認)<input class="boxA" type="text" name="passwordB">
				</p>
				<p>
					ユーザー名<input class="box" type="text" name="userName">
				</p>
				<p>
					生年月日<input class="box" type="date" name="date">
				</p>
				<input type="submit" value="登録">
			</div>
		</div>


	</form>



	</main>
	<footer>
		<a class="return"
			href="UserListServlet">
			戻る </a>

	</footer>



</body>
</html>