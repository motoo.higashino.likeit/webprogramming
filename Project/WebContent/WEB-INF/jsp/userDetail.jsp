<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>ユーザ一覧画面</title>
    <!-- BootstrapのCSS読み込み -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
    <!-- Jqeryの読み込み -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <!-- BootstrapのJS読み込み -->
    <script src="js/bootstrap.min.js"></script>
    <!-- レイアウトカスタマイズ用個別CSS -->
<meta charset="UTF-8">
    <link rel="stylesheet" href="css/userDetail.css" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>ユーザー詳細画面</title>
</head>
<body>
    <header>
    <div class="header-name">
        <!--あとで利用者の名前を出力するように変更する-->
        ${User.name } さん
        <!-- TODO ログアウトサーブレットにリンク -->
			> <a class="logout" href="LogoutServlet">ログアウト </a> ログアウト </a>
        </div>

    </header>
    <main>
        <h1 class="detail-title">
        ユーザー情報詳細参照
        </h1>
        <!-- TODO スコープから詳細情報を取得し表示 -->
        <div class="detail">
            <p>ログインID&emsp;${user.loginId}</p>
            <p>ユーザー名&emsp;${user.name}</p>
            <p>生年月日&emsp;${user.birthDate}</p>
            <p>登録日時&emsp;${user.createDate}</p>
            <p>更新日時&emsp;${user.updateDate}</p>

        </div>



    </main>


    <footer>
    <!-- TODO 戻るでユーザー一覧にリダイレクト -->>
    <a class="return" href="UserListServlet">
       戻る
        </a>

    </footer>

</body>
</html>