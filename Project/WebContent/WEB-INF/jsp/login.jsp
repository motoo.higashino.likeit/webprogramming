<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/login.css" type="text/css">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<title>ログイン画面</title>
</head>
<body>
	<h1 class="logIn-title">ログイン画面</h1>
	<h4>${errMsg }</h4>
	<form action="LoginServlet" method="post">
		<div class="logIn">
			<p>
				ログインID<input type="text" name="loginId">
			</p>
			<p>
				パスワード<input type="text" name="password">
			</p>
			<input type="submit" value="ログイン">
		</div>



	</form>


</body>
</html>