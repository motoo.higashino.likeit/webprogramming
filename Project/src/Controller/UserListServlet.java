package Controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserListServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ユーザー一覧画面へ遷移・ユーザーテーブルの情報を表示（管理者は表示しない）
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//初期状態ではすべてのユーザー情報を表示
		UserDao userDao = new UserDao();
		ArrayList<model.User> userList = userDao.userList();

		//スコープにセット
		request.setAttribute("userList", userList);

		//ユーザー一覧画面に遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//検索機能
		//ログインID・ユーザー名・生年月日で検索
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//パラメーターを取得し、データベースからデータ取得
		String loginId = request.getParameter("loginId");
		String userName = request.getParameter("userName");
		String dateA = request.getParameter("dateA");
		String dateB = request.getParameter("dateB");

		UserDao userDao = new UserDao();
		ArrayList<model.User> userList = userDao.searchList(loginId,userName,dateA,dateB);

		//スコープにセット
		request.setAttribute("userList", userList);

		//ユーザー一覧画面に遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}

}
