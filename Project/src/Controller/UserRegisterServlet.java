package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;

/**
 * Servlet implementation class UserRegister
 */
@WebServlet("/UserRegisterServlet")
public class UserRegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//ユーザー登録画面へ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		//登録したパラメーターをリクエスト
		String loginId= request.getParameter("loginId");
		String passwordA = request.getParameter("passwordA");
		String passwordB =request.getParameter("passwordB");
		String userName = request.getParameter("userName");
		String dateA = request.getParameter("date");

		//データベースを参照
		UserDao userDao = new UserDao();

		//すでログインIDがあった場合
		if(userDao.idCheck(loginId)!=null) {
			//エラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			//ユーザー登録画面へ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
			//パスワードが一致しない
			return;
		}
		if (!(passwordA.equals(passwordB))) {
			//エラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			//ユーザー登録画面へ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
			//未入力の項目がある場合
			return;
		}
		if (loginId.equals("")||passwordA.equals("")||passwordB.equals("")||userName.equals("")||dateA.equals("")) {
			//エラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			//ユーザー登録画面へ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ユーザー情報を登録
		userDao.insertUser(loginId, passwordA, userName, dateA);

		//ユーザー一覧画面へ遷移
//		RequestDispatcher dispatcher = request.getRequestDispatcher("UserListServlet");
//		dispatcher.forward(request, response);

		response.sendRedirect("UserListServlet");




	}

}
