package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdateServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//keyでデータベースからデータを取得しテキストボックスに表示する
		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//userList.jspからidを取得
		String id = request.getParameter("id");

		//Keyを使ってデータベースから詳細情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.userDetail(id);

		//データをスコープにセット
		request.setAttribute("id", id);
		request.setAttribute("loginId", user.getLoginId());
		request.setAttribute("name", user.getName());
		request.setAttribute("birthDate", user.getBirthDate());

		//ユーザー詳細画面へ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/* (非 Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//入力内容をデータベースに更新する

		//文字化け防止
		request.setCharacterEncoding("UTF-8");

		//入力事項を取得
		String passwordA = request.getParameter("passwordA");
		String passwordB = request.getParameter("passwordB");
		String userName = request.getParameter("userName");
		String birthDate = request.getParameter("birthDate");

		//エラー処理
		//パスワードあり
		if (!(passwordA.equals("")) || !(passwordB.equals(""))) {
			//パスワードの一致を確認
			if (!(passwordA.equals(passwordB))) {
				request.setAttribute("errMsg", "入力された内容は正しくありません");
				//ユーザー登録画面へ遷移
				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
				dispatcher.forward(request, response);
			}
		}

		//ユーザー名・生年月日の""を確認
		if (userName.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			//ユーザー登録画面へ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegister.jsp");
			dispatcher.forward(request, response);
		}

		//セッションからIDを取得
		String id = request.getParameter("id");

		UserDao userDao = new UserDao();

		if (passwordA.equals("") && passwordB.equals("")) {
			//ユーザー名・生年月日を更新
			userDao.userUpdateD(id, userName, birthDate);

		} else if (passwordA.equals(passwordB)) {
			//パスワード・ユーザー名・生年月日を更新
			userDao.userUpdateT(id, userName, birthDate, passwordA);

		}

		//ユーザー一覧画面に遷移
		response.sendRedirect("UserListServlet");



	}

}
