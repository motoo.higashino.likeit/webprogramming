package Controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		//ユーザー一覧からKeyを取得し、ログインIDを取得
		//userList.jspからidを取得
		String id = request.getParameter("id");

		//Keyを使ってデータベースから詳細情報を取得
		UserDao userDao = new UserDao();
		User user=userDao.userDetail(id);
		//代入した情報をスコープにセット
		request.setAttribute("user", user);
		request.setAttribute("id", id);

		//ユーザー削除画面へ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//文字化け防止
		//IDを使ってデータベースからユーザー情報を削除
		//セッションからIDを取得
		String id = request.getParameter("id");

		//ID指定でデータを削除
		UserDao userDao=new UserDao();
		userDao.userDelete(id);

		//ユーザー一覧へ遷移
		response.sendRedirect("UserListServlet");


	}

}
