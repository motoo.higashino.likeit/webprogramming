package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public String MD5(String password) {
		try {
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String passwordMD5 = DatatypeConverter.printHexBinary(bytes);

		return passwordMD5;

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		return null;
	}

	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String passwordMD5 = MD5(password);

			//select文を準備
			String sql = "SELECT * FROM user WHERE login_id =? and password =?";

			//引数をselect文に代入
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, loginId);
			pstmt.setString(2, passwordMD5);
			ResultSet rs = pstmt.executeQuery();

			// ログイン失敗時の処理		・・・①
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理		・・・②
			int id = rs.getInt("id");
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData, id);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return null;
	}

	//ユーザー登録時にlogin_idの有無を確認するメソッド
	public ResultSet idCheck(String loginId) {
		Connection conn = null;
		try {
			//データベースに接続
			conn = DBManager.getConnection();

			//Select文を準備
			String idCheck = "SELECT login_id FROM user WHERE login_id = ?";
			PreparedStatement pstmt = conn.prepareStatement(idCheck);
			pstmt.setString(1, loginId);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			return rs;

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		return null;
	}

	public void insertUser(String loginId, String password, String userName, String dateA) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String passwordMD5 = MD5(password);

			String insert = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date) VALUES (?,?,?,?,now(),now())";
			PreparedStatement pstmt = conn.prepareStatement(insert);
			pstmt.setString(1, loginId);
			pstmt.setString(2, userName);
			pstmt.setString(3, dateA);
			pstmt.setString(4, passwordMD5);

			//アップデート文
			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//UserDetailServletからKeyを引数にデータベースから情報を取得　情報を返す
	public User userDetail(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String userDetail = "SELECT * FROM user WHERE id=?";

			PreparedStatement pstmt = conn.prepareStatement(userDetail);
			pstmt.setString(1, id);
			ResultSet rs = pstmt.executeQuery();

			if (!rs.next()) {
				return null;
			}

			//データの取り出し
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User user = new User(loginId, name, birthDate, createDate, updateDate);

			return user;

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
		return null;
	}

	//idをKeyにpass.name.birthを更新・name.birthを更新する2つのメソッドを作る
	public void userUpdateD(String id, String userName, String birthDate) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String updateD = "UPDATE user SET name=?, birth_date=?,update_date=now() WHERE id=?";

			PreparedStatement pstmt = conn.prepareStatement(updateD);
			pstmt.setString(1, userName);
			pstmt.setString(2, birthDate);
			pstmt.setString(3, id);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	public void userUpdateT(String id, String userName, String birtDate, String password) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String passwordMD5 = MD5(password);

			String updateD = "UPDATE user SET name=?,birth_date=?,password=? WHERE id=?";

			PreparedStatement pstmt = conn.prepareStatement(updateD);
			pstmt.setString(1, userName);
			pstmt.setString(2, birtDate);
			pstmt.setString(3, passwordMD5);
			pstmt.setString(4, id);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//ID指定でユーザー情報を削除する
	public void userDelete(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String userDelete = "DELETE FROM user WHERE id=?";

			PreparedStatement pstmt = conn.prepareStatement(userDelete);
			pstmt.setString(1, id);

			pstmt.executeUpdate();

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}
	}

	//continueを使ってloginId==adminのみ出力処理を省く
	//他の user table の中データを全て出力 しリストに入れ、リストをかえす
	//取り出すデータ login_id,name,birth_date の3個
	public ArrayList<User> userList() {
		Connection conn = null;
		ArrayList<User> userList = new ArrayList<User>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT id,login_id,name,birth_date FROM user WHERE login_id !='admin'";

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String birthDate = rs.getString("birth_date");

				User user = new User(id, loginId, name, birthDate);
				userList.add(user);

			}
			return userList;

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		return null;
	}

	public ArrayList<User> searchList(String loginId, String name, String dateA, String dateB) {
		Connection conn = null;
		ArrayList<User> userList = new ArrayList<User>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT id,login_id,name,birth_date FROM user WHERE login_id !='admin'";
			if (!loginId.equals("")) {
				sql += " AND login_id = '" + loginId + "'";
			}

			if (!name.equals("")) {
				sql += " AND name LIKE '%" + name + "%'";
			}

			if (!dateA.equals("")) {
				sql += " AND birth_date >=" + "'" + dateA + "'";
			}

			if (!dateB.equals("")) {
				sql += " AND birth_date <=" + "'" + dateB + "'";
			}

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId1 = rs.getString("login_id");
				String name1 = rs.getString("name");
				String birthDate = rs.getString("birth_date");

				User user = new User(id, loginId1, name1, birthDate);
				userList.add(user);

			}
			return userList;

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}

		}

		return null;
	}

}
